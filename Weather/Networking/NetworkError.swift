//
//  NetworkError.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case unknown
    case timeout
}
