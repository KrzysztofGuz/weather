//
//  GetWeather.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import RxSwift

protocol GetWeatherProtocol {
    func getWeatherFor(city: String) -> Observable<WeatherResult?>
}

class GetWeatherHandler: GetWeatherProtocol {
    private let networkingManager: NetworkingManager

    init(_ networkingManager: NetworkingManager = NetworkManager()) {
        self.networkingManager = networkingManager
    }
    
    func getWeatherFor(city: String) -> Observable<WeatherResult?> {
        guard let url = URL.urlForWeatherAPI else { return Observable.error(NetworkError.badURL) }

        let parameters: [String: String] = ["q": city,
                                            "units": ApiKey.unitMetric,
                                         "APPID": ApiKey.appId]

        let resource = Resource<WeatherResult>(url: url, parameter: parameters)

        return networkingManager.load(resource: resource)
            .map { article -> WeatherResult? in
                article
            }
            .asObservable()
    }
}
