//
//  URL+Extensions.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import Foundation

extension URL {
    
    static var urlForWeatherAPI: URL? { return URL(string: ApiKey.baseURL + "data/2.5/weather") }
}

struct ApiKey {
    static let appId = "aec6defeae912e2f8460f5179ac00fb8"
    static let baseURL = "https://api.openweathermap.org/"
    static let unitMetric = "metric"
    static let unitImperial = "imperial"
}
