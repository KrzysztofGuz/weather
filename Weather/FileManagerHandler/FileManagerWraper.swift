//
//  FileManagerWraper.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import Foundation
import RxSwift

protocol AddCityListHandlerProtocol {
    func getSearchCityList() -> Observable<[CityListModel]>
}

final class FileManagerWraper: AddCityListHandlerProtocol {
    private let fileManagerHandler: FileManagerHandlerProtocol!

    init(withFileManagerHandler fileManagerHandler: FileManagerHandlerProtocol = FileManagerHandler()) {
        self.fileManagerHandler = fileManagerHandler
    }

    func getSearchCityList() -> Observable<[CityListModel]> {
        let resource: FileManagerResource<[CityListModel]> = FileManagerResource(fileName: "cityList")
        return fileManagerHandler.load(resource: resource)
    }
}
