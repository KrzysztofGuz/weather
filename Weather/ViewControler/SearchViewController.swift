//
//  MainViewController.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import RxCocoa
import RxSwift
import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!

    private var cityList = [CityListModel]()
    private let cityNameValidator = CityNameValidator()
    private var viewModel = CitySearchViewModel()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewModel()
    }

    private func configureViewModel() {
        viewModel.cityList
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] list in
                self?.cityList = list
                self?.tableView?.reloadData()
            }).disposed(by: disposeBag)

        viewModel.isLoading.bind(to: activityIndicatorView.rx.isAnimating).disposed(by: disposeBag)
        
        let searchQuery = searchBar.rx.text.orEmpty.asObservable()
        viewModel.searchCityWithName(withName: searchQuery)
        viewModel.getCityList()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCityWeather" {
            if let viewController = segue.destination as? WeatherViewController {
                viewController.selectedCity = sender as? String
            }
        }
    }

}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cityList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath)
        cell.textLabel?.text = cityList[indexPath.row].name
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCityName = self.cityList[indexPath.row].name
        performSegue(withIdentifier: "showCityWeather", sender: selectedCityName)
    }
}

