//
//  WeatherViewModelProtocol.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import RxSwift

protocol WeatherViewModelProtocol {
    var weatherList: Observable<WeatherResult> { get }
    func getWeatherFor(city: String)
}
