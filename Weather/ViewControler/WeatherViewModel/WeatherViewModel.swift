//
//  WeatherViewModel.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import RxSwift

final class WeatherViewModel: WeatherViewModelProtocol {
    private let getWeatherHandler: GetWeatherProtocol
    private let disposeBag = DisposeBag()
    var weatherList: Observable<WeatherResult>

    private let weatherListSubject = PublishSubject<WeatherResult>()

    init(withGetWeather getWeatherHandler: GetWeatherProtocol = GetWeatherHandler()) {
        self.getWeatherHandler = getWeatherHandler
        self.weatherList = weatherListSubject.asObserver()
    }

    func getWeatherFor(city: String) {
        getWeatherHandler.getWeatherFor(city: city)
            .catchError { error -> Observable<WeatherResult?> in
                print(error.localizedDescription)
                return Observable.just(WeatherResult.empty)
            }
            .subscribe(onNext: { [weak self] result in
                if let result = result {
                    self?.weatherListSubject.onNext(result)
                }
            }, onError: { error in
                print("getWeather onError: \(error)")
            })
            .disposed(by: disposeBag)
    }
}
