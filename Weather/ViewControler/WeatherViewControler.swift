//
//  WeatherViewControler.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

import RxCocoa
import RxSwift
import UIKit

final class WeatherViewController: UIViewController {
    private let disposeBag = DisposeBag()
    var viewModel: WeatherViewModelProtocol = WeatherViewModel()
    var selectedCity: String?

    @IBOutlet var cityNameLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let selectedCityString =  selectedCity else {
            return
        }
        self.cityNameLabel.text = selectedCityString
        self.setupViewModel(with: selectedCityString)
    }

    private func setupViewModel(with selectedCity: String) {
        self.viewModel.getWeatherFor(city: selectedCity)

        self.viewModel.weatherList
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] weather in
                self?.displayWeather(weather)
            }, onError: { error in
                print("onError: \(error)")
            })
            .disposed(by: disposeBag)
    }

    private func displayWeather(_ weather: WeatherResult?) {
        if let weather = weather?.main {
            self.temperatureLabel.textColor = getTemperatureFontColor(for: weather.temp)
            self.temperatureLabel.text = "Temperature: \(weather.temp) °C"
            self.humidityLabel.text = "Humidity: \(weather.humidity)"
        } else {
            self.temperatureLabel.text = ""
            self.humidityLabel.text = ""
        }
    }
    
    private func getTemperatureFontColor(for temperature: Double) -> UIColor {
        switch temperature {
        case _ where temperature < 10:
            return UIColor.blue
        case _ where temperature >= 10 && temperature <= 20:
            return UIColor.black
        case _ where temperature > 20:
            return UIColor.red
        default:
            return UIColor.black
        }
    }
}
