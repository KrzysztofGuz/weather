//
//  CityListModel.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

struct CityListModel: Codable {
    let id: Int?
    let name: String?
    let country: String?
}
