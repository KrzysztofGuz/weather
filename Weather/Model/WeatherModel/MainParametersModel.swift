//
//  MainParametersModel.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

struct MainParametersModel: Codable {
    let temp: Double
    let pressure: Double
    let humidity: Double
    let temp_min: Double
    let temp_max: Double
}
