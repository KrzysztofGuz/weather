//
//  WeatherResult.swift
//  Weather
//
//  Created by Krzysztof on 12/04/2021.
//

struct WeatherResult: Codable {
    let name: String?
    let main: MainParametersModel?
}

extension WeatherResult {
    static var empty: WeatherResult {
        return WeatherResult(name: nil, main: nil)
    }
}
